var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/database');
require('../config/passport')(passport);
var express = require('express');
var jwt = require('jsonwebtoken');
var router = express.Router();
var User = require("../models/user");
var Workhour = require('../models/workhour');


exports.getAllWorkhours = function(req, res) {
	checkToken(req, res);
    Workhour.find({user_id: req.user._id}, function(err, workhours) {
      if(err) {
        return res.json({success:false, msg: 'Something went wrong!'});
      }
      else {
        res.json({workhours: workhours});
      }
    })

}

exports.getWorkhour = function(req, res) {
	checkToken(req, res);
    Workhour.find({user_id: req.user._id, _id: req.params.id}, function(err, workhours) {
      if(err) {
        return res.json({success:false, msg: 'Something went wrong!'});
      }
      else {
        res.json({workhours: workhours});
      }
    })
}

exports.deleteWorkhour = function(req, res) {
	checkToken(req, res);
    console.log(req.body);
    Workhour.remove({user_id: req.user._id, _id: req.body._id}, function(err) {
      if(err) {
        return res.json({success:false, msg: 'Delete workhour failed!'});
      }
      else {
        res.json({success:true, msg:'Successfuly deleted workhour'});
      }
    })
}

exports.addWorkhour = function(req, res) {
	checkToken(req, res);
    var workhour = new Workhour();
    workhour.user_id = req.user._id;
    workhour.description = req.body.description;
    workhour.interval = req.body.interval;
    //workhour.date = req.body.date;

    workhour.save(function(err) {
      if(err) {
        return res.json({success:false, msg:'Save workhour failed.'});
      }
      else {
        res.json({success:true, msg:'Successful created new workhour'});
      }
    })
}

//Helper functions
getToken = function (headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } 
    else {
      return null;
    }
  } 
  else {
    return null;
  }
};

checkToken = function(req, res) {
	var token = getToken(req.headers);
  	if (!token) {
  		return res.status(403).send({success: false, msg: 'Unauthorized.'});
  	}
};