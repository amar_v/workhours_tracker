var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WorkhourSchema = new Schema({
  user_id: {
    type: String,
    required: true
  },
  description: {
    type: String,
    default: ''
  },
  interval: {
    type: Number,
    default: 0
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Workhour', WorkhourSchema);