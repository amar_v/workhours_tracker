var express = require('express');
var router = express.Router();
var UserController = require('../controllers/userController');

router.post('/signup', UserController.signUp);

router.post('/signin', UserController.signIn);

module.exports = router;
