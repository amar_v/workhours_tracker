var passport = require('passport');
require('../config/passport')(passport);
var express = require('express');
var router = express.Router();
var WorkhourController = require('../controllers/workhourController.js');

router.get('/', passport.authenticate('jwt', { session: false}), WorkhourController.getAllWorkhours);

router.get('/:id', passport.authenticate('jwt', { session: false}), WorkhourController.getWorkhour);

router.delete('/', passport.authenticate('jwt', { session: false}), WorkhourController.deleteWorkhour);

router.post('/', passport.authenticate('jwt', { session: false}), WorkhourController.addWorkhour);

module.exports = router;